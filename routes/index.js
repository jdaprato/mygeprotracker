const Router = require('express').Router();

Router.use('/projects', require('./projects'));
Router.use('/users', require('./users'));

// Global error handler
Router.use((err, req, res, next) => {

    if (err.name === 'APIError') {
        return res.status(err.status).json({
            success: false,
            error_code: err.status,
            error_message: err.message,
        });
    }

    res.status(500);
    res.json({
        success: false,
        error_code: 500,
        error_message: err.message,
    });

    next(err);
});

module.exports = Router;

