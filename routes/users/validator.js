const {check} = require('express-validator');
const { Mongoose } = require('mongoose');

module.exports = {
    post: [
        check('lastname', 'The lastname must be a String').isString(),
        check('lastname', 'The lastname must not be empty').notEmpty(),
        check('firstname', 'The firstname must be a String').isString(),
        check('lastname', 'The lastname must not be empty').notEmpty(),
        check('email', 'Email must not be empty').notEmpty(),
        check('email', 'Email must be a valid email').normalizeEmail().isEmail(),
        check('active', 'Active must not be empty'),
        check('active', 'Active must be a boolean').isBoolean()
    ]
}