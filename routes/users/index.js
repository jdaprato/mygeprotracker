const Router = require('express').Router();
const Controller = require('./controller');

Router.post('/', Controller.post);
Router.get('/', Controller.get);

module.exports = Router;