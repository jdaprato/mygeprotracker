const Users = require('../../models/User');
const { response } = require('express');

module.exports = {
    get: async function(req, res, next) {
        try {
            let user = await (await Users.findOne({ _id: req.params.id }));
            return res.status(200).json({user: user});
        } catch(e) {
            next(e);
        }
    },
    post: async function(req, res, next) {
        try {
            const newUser = new Users({
                lastname: req.body.lastname,
                firstname: req.body.firstname,
                email: req.body.email,
                active: true
            });

            await newUser.save();
            res.status(201).json({user: newUser});
        } catch(e) {
            next(e);
        }
    }
}