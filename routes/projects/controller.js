const Projects = require('../../models/Project');
const Users = require('../../models/User');

module.exports = {
    list: async function (req, res, next) {
        try {
            let projects = await Projects.find({}).populate('users').populate('tickets');;
            return res.status(200).json({ projects: projects });
        } catch (e) {
            next(e);
        }
    },
    get: async function (req, res, next) {
        try {
            let project = await (await Projects.findOne({ _id: req.params.id })).populate('users').populate('tickets');
            if(!projects){
                return next(new APIError(404, 'Could not find any project with given id'));
            }
            return res.status(200).json({project: project});
        } catch (e) {
            next(e);
        }
    },
    post: async function(req, res, next) {
        try {
            let users = await Users.findOne({_id: req.body.user});
            if(!users){
                return next(new APIError(404, "could not find given user"));
            }

            const newProject = new Projects({
                name: req.body.name,
                technicalName: req.body.technicalName,
                description: req.body.description,
                users: [],
                database: req.body.database,
                ORM: req.body.orm,
                language: req.body.language,
                sourceControlManagement: req.body.sourceControlManagement,
                status: "created",
                active: true
            });

            await newProject.save();
            res.status(201).json({project: newProject});

        } catch(e){
            next(e);
        }
    }

}