const Router = require('express').Router();
const Controller = require('./controller');

Router.get('/', Controller.list);
Router.get('/', Controller.get);
Router.post('/', Controller.post);

module.exports = Router;