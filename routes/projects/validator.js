const {check} = require('express-validator');
const { Mongoose } = require('mongoose');
const ObjectId = require('mongoose').Types.ObjectId;

module.exports = {
    post: [
        check('name', 'name is required').not().isEmpty(),
        check('name', 'name must be a String').isString(),
        check('technicalName', 'technicalName must be a string').isString(),
        check('description', 'description must be a string').isString(),
        //check('users', 'users must be an array').isArray(),
        /*check('users.*', 'users must contains a valid ObjectID').custom((users) => {
            return ObjectId.isValid(users);
        }),*/
        check('database', 'database must be a string').isString(),
        check('database').isIn(['MySQL', 'PostgreSQL', 'MongoDB']),
        check('ORM', 'ORM must be a string').isString(),
        check('ORM').isIn(['ActiveRecord', 'Mongoose', 'Doctrine', 'Spring Data', 'Hibernate']),
        check('language', 'language must be a string').isString(),
        check('language').isIn(['PHP', 'NodeJS', 'Java', 'Ruby']),
        check('sourceControlManagement', 'sourceControlManagement must be a string').isString(),
        check('status', 'status must be a string').isString(),
        check('active', 'active must be a boolean').isBoolean(),
    ]
}