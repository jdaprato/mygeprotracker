const express = require('express');
const app = express();
const helmet = require('helmet');
const session = require('express-session');
const mongoose = require('mongoose');


try {
    mongoose.connect('mongodb://localhost:27017/mygetprotracker', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
} catch(e){
    console.log(e);
}

app.use(helmet()); // Set HTTP haders
app.set('trust proxy', 1);

app.use(session({
    secret : 'secret',
    name: 'sessionId',
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: false, 
        httpOnly: true,
        expires: new Date(Date.now() + 60 *60 * 1000)
    }
}));



app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use('/api', require('./routes'));

module.exports = app;