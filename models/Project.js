const mongoose =  require('mongoose');
const {Schema} = mongoose;

const ProjectsSchema = new Schema(
    {
        name: {type: String},
        technicalName: {type: String},
        description: {type: String},
        tickets: {type: Schema.Types.ObjectId, ref: 'Tickets'},
        users: {type: Schema.Types.ObjectId, ref: 'Users'},
        database: {type: String},
        ORM: {type: String},
        language: {type: String},
        sourceControlManagement: {type: String},
        status: {type: String},
        active: {type: Boolean}
    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model('Projects', ProjectsSchema);

