const mongoose = require('mongoose');
const { Schema } = mongoose;

const UsersSchema = new Schema(
    {
        lastname: { type: String },
        firstname: { type: String },
        email: { type: String },
        active: { type : Boolean }
    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model('Users', UsersSchema);